const supertest = require('supertest')
const jwt = require('jsonwebtoken')
const chai = require('chai')
const assert = require('assert')
const rsa = require('node-rsa')
chai.config.showDiff = true

const app = require('../app')
const expect = chai.expect

const {
    users
} = require('../db/database.js/database')

const mockPublicKey = `-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAw0Oha22qPz4HLvdck2Qr
WCzE/LdNa8dfXmVItYBvzmBa411BJbuUR6yXZOfe7X6XtfE0NIQgGBNk7wcWyaIw
m7+fFYJj2dxN/LOCeFDo5YH0/xRRxLXhLc/G92EJgz4/Avaotw5tqFNoVGMjL8JZ
QgSfTkIrnV2PJ7tyi6w8jMKeO4yYxLhKsym54xuSjX8AQtN5ELY+SPETGwfgIS6m
QBy6x/pgmNqRn89rS9LeBhPrTJR94kzMyr0LzYHIKEim6JuaZ0B3L/y1XCU6E7fk
0s4H5c45Ik8xR5XJZbx/HSOXXBkXQ/NAhqFkEPDtJEt/pgJ8EUBEKabh3pfRViKp
NwIDAQAB
-----END PUBLIC KEY-----
`

const mockPrivateKey = `-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAw0Oha22qPz4HLvdck2QrWCzE/LdNa8dfXmVItYBvzmBa411B
JbuUR6yXZOfe7X6XtfE0NIQgGBNk7wcWyaIwm7+fFYJj2dxN/LOCeFDo5YH0/xRR
xLXhLc/G92EJgz4/Avaotw5tqFNoVGMjL8JZQgSfTkIrnV2PJ7tyi6w8jMKeO4yY
xLhKsym54xuSjX8AQtN5ELY+SPETGwfgIS6mQBy6x/pgmNqRn89rS9LeBhPrTJR9
4kzMyr0LzYHIKEim6JuaZ0B3L/y1XCU6E7fk0s4H5c45Ik8xR5XJZbx/HSOXXBkX
Q/NAhqFkEPDtJEt/pgJ8EUBEKabh3pfRViKpNwIDAQABAoIBAQCyosimM2mw+dow
iXRUEyvkOvDsEfXuSDNjdF2UTq/6qgP8RXA4SOdauva2BbrYoxvAA66y7L0/yV5p
np6mUI8PrbqoPuWChh2CtecCk2K9tKqiQcCcqZgcBR4NauT9AJE88NNgxP5lJAkq
SpgCNVbbIsdrHPYxvUoeG0qrpSKv0jMTASZRwFEi5Il74Y3bn7+mkyzHWKLfCgiF
WEWzKjfiL+R5WKgnVREv2ivv8yR9XPdqarQ9nHaGpaHx87mLV2LkNp6N1jauAIQX
Xmy9BN0LxBs+yru3uPS/VmUYiA6Gt4QyghaONKAKg5qOKYCF7K8moJpJKuQ/K8hp
Oh9eo28BAoGBAOZqW1JTuCkcgwhJEmKSVk/GL7Ja8OFqhQUjdVs0y4quRQ6VMRdz
gBYwHuME45SI8BBCa1YfZOtnVvycZV/aYz+WByD2FtOVPtxQLx+bY2x8CzSLDj4n
FbGETGepP6uwqP804aKGkCmCdu7mSKM/HdehBBxO1+cU7Mo7GVXl6oghAoGBANjy
Fgc4DBdlRJ+ZM/9eYdbnttbfjsSpFHUPSr1N4CGl7bXk4hzKkoRL5PMDUnbMOkKc
TGDbEeCoXR/Yh/luHt2UosZhwQJ+2s4iNGzG6+tmE36uOXz16PxasoT5qQxEvnRh
dsf3JwNGwlafGmIdsDAgY8TyMLJMHVdftoiWCaZXAoGAPq/U5JMOWXD+XTS0Ick6
Xj3HIDkJq+o1Am7hMM0uSy6abxs73et4f5Db9luQJwsSdPn9BdNIu5rD2bobAncB
5XzFt7v7/dhu37WY/HpkFFGzIrpqR5KOnlsmBlXPksBfWf9jG7qMYh6yiNByFqfH
ULDn02PH7YCN438sp2gD5MECgYBuFHiE9TV9cY8ehHhT7aorh6vI3+rrcvfEZhwA
blF6AV3a2+a3wToTEwd7H4ZqbTpqrjA7PS2XFxcZn59vOydCO7dUfimXgJK54bVE
NLNk4Iin0POuqoCr+8F1o9DZPtB88S96S7hp+y+FEHosqu/Jm8XEGzgGYITK3vAZ
4jfHRQKBgB3py1mEh+saXHIRoX/wpFNBtSiqOh91ZCI+vHGM148XTJATGpkEpyoN
z3V3w43BxmyrH4GM5FyTD6p5AaIKlZKrzjqNMnO+OVd1OVw3suUdAXDZE6lYv/8W
PFp4dCCI4eqcVT319r0lj+vFxgSU//2P2yYa8E1KDsu0FvqDFBa4
-----END RSA PRIVATE KEY-----
`

const testUser = {
    username: "pepegles",
    pubKey: Buffer.from(mockPublicKey, 'utf-8').toString('base64')
}

describe("Login", () => {

    before(async () => {
            await users.remove({})
        }
    )

    it("allow register", async () => {
        const response = await supertest(app).post('/register').send(
            testUser
        ).expect(200)
    })
})

describe("rsa", () => {

    before(async () => {
        await users.remove({})
    })

    it("accept challenge", async () => {
        await supertest(app).post('/register').send(
            testUser
        ).expect(200)

        const response = await supertest(app).post('/request').send(
            {username: testUser.username}
        ).expect(200)

        const challenge = response.body.challenge

        const key = new rsa(mockPrivateKey)
        const signature = key.sign(challenge, 'hex', 'hex')
        const response2 = await supertest(app).post('/submit').send(
            {username: testUser.username, signature: signature}
        ).expect(200)

        console.log(response2.body)
    })
})