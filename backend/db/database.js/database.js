const monk = require('monk');

const dbUser = process.env.DB_USER;
const dbPass = process.env.DB_PASS;

const connUrl = `mongodb://${dbUser}:${dbPass}@localhost:27017/`
console.log(connUrl)
const db = monk(connUrl);

const users = db.get('users');
// users.createIndex('username', {unique: true})

const challenges = db.get('challenges');
// challenges.createIndex('username', {unique: true})

module.exports = {users, challenges};