const jwt = require('jsonwebtoken')
const rsa = require('node-rsa')
const crypto = require('crypto')
const {verify} = require('hcaptcha')

const {users, challenges} = require('../db/database.js/database')

const insertNewUser = (req, res, next) => {
    const {username, pubKey} = req.validatedBody

    console.log(req.validatedBody)

    const newUserData = {
        username: username,
        pubKey: Buffer.from(pubKey, 'base64').toString('hex'),
        keys: []
    }

    console.log(newUserData)

    users.insert(newUserData).then((_) => {
        res.status(200).end()
    }).catch((err) => {
        next(err)
    })
}

const generateValidWebToken = (req, res, next) => {
    jwt.sign({
        username: req.validatedBody.username,
        _id: req.validatedBody._id,
    }, process.env.SERVER_SECRET, {
        expiresIn: '100d'
    }, (err, jwt) => {
        if (err) {
            next(err)
        } else {
            res.json({token: jwt})
            next()
        }
    })
}

const checkValidWebToken = (req, res, next) => {
    const auth_header = req.headers.authorization

    if (auth_header) {
        const token = auth_header.split(' ')[1]
        jwt.verify(token, process.env.SERVER_SECRET, (err, decoded) => {
            if (err) {
                next(err)
            } else {
                req.validJWT = decoded
                next()
            }
        })
    } else {
        const error = new Error("Missing Auth header")
        next(error)
    }
}

const getUserSecretKeys = (req, res, _next) => {
    const {username} = req.validJWT

    const dbQuery = {
        username: username
    }

    users.findOne(dbQuery, {fields: {keys: 1}}).then((value) => {
        res.json(value)
    })
}

const addKeyToDatabase = (req, res, next) => {
    const {keyName, keyType, keyValue} = req.validatedBody
    const {username} = req.validJWT

    const dbQuery = {
        username: username
    }

    users.findOneAndUpdate(dbQuery, {
        $addToSet: {
            keys: {keyName, keyType, keyValue}
        }
    }).then(() => {
        res.status(200).end()
    }).catch((err) => {
        next(err)
    })
}

const removeKeyFromDatabase = (req, res, next) => {
    const {keyName, keyType, keyValue} = req.validatedBody
    const {username} = req.validJWT

    const dbQuery = {
        username: username
    }

    users.findOneAndUpdate(dbQuery, {
        $pull: {
            keys: {keyName, keyType, keyValue}
        }
    }).then((v) => {
        console.log(v)
        res.status(200).end()
    }).catch((err) => {
        next(err)
    })
}

const verifyCaptcha = (req, res, next) => {
    const {token} = req.validatedBody.captcha

    verify(process.env.CAPTCHA_SECRET, token).then(data => {
        next()
        // TODO: Captcha
        // if (data.success) {
        //     next()
        // } else {
        //     next("Captcha failed")
        // }
    }).catch(err => {
        next(err)
    })

}

const generateAuthChallenge = (req, res, next) => {
    crypto.randomBytes(128, (err, buf) => {
            if (err) {
                next(err)
            } else {
                const challengeString = buf.toString('hex')
                const {username} = req.validatedBody

                const dbQuery = {
                    username: username
                }

                challenges.findOneAndUpdate(dbQuery, {
                    $setOnInsert: {
                        username: username,
                        timeStamp: Date.now(),
                        challengeString: challengeString
                    }
                }, {upsert: true}).then((value) => {
                    res.json({challenge: value.challengeString})
                }).catch((err) => {
                    next(err)
                })
            }
        }
    )
}

const checkAuthChallenge = (req, res, next) => {
    const {username, signature} = req.validatedBody


    const dbQuery = {
        username: username
    }

    challenges.findOne(dbQuery).then((value => {
            if (!value) {
                next(Error("Challenge not created"))
            } else {

                const currentTimestamp = Date.now()

                if ((currentTimestamp - value.timestamp) > 10000000) {
                    challenges.remove(dbQuery)
                        .then((_) => {
                            challenges.remove(dbQuery).then((_) => {
                                next(Error("Too old"))
                            }).catch((err) => {
                                next(err)
                            })
                        })
                        .catch((err) => {
                            next(err)
                        })
                } else {
                    users.findOne(dbQuery).then((user) => {
                        if (!user) {
                            next(Error("Not found"))
                        } else {
                            const validationKey = new rsa(Buffer.from(user.pubKey, 'hex').toString('utf-8'))

                            const challengeBuf = Buffer.from(value.challengeString, 'hex')
                            console.log(challengeBuf, signature)
                            const valid = validationKey.verify(challengeBuf,
                                signature, 'hex', 'hex')
                            if (valid === true) {
                                challenges.remove(dbQuery).then((_) => {
                                    req.hasValidSignature = true;
                                    next()
                                }).catch(err => {
                                    next(err)
                                })
                            } else {
                                next(Error("Invalid signature"))
                            }
                        }
                    }).catch((err) => {
                        next(err)
                    })
                }
            }
        }
    )).catch((err) => {
        next(err)
    })
}

const removeChallengeIfValid = (req, res, next) => {
    const {username} = req.validatedBody

    const dbQuery = {
        username: username
    }

    users.findOneAndUpdate(dbQuery, {
        $unset: {
            activeChallenge: true
        }
    }).then((_) => {
        next()
    }).catch((err) => {
        next(err)
    })
}

module.exports = {
    insertNewUser,
    generateValidWebToken,
    checkValidWebToken,
    addKeyToDatabase,
    removeKeyFromDatabase,
    getUserSecretKeys,
    removeChallengeIfValid,
    checkAuthChallenge,
    generateAuthChallenge,
    verifyCaptcha
}