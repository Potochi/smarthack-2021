const joi = require('joi')

const usernameSchema = joi.string().alphanum().min(4).max(32)
const pubKeySchema = joi.string()
const signatureSchema = joi.string()

const jsonSchemaValidator = (schema, errorCode) => (req, res, next) => {
    const {error, value} = schema.validate(req.body)

    if (error) {
        res.statusCode = errorCode
        next(error)
    } else {
        req.validatedBody = value
        next()
    }
}


const KeyType = {
    AES128: "AES128",
    AES192: "AES192",
    AES256: "AES256",
    TRIPLEDES: "TRIPLEDES",
    ECC256: "ECC256",
    ECC384: "ECC384",
    PASSWORD: "PASSWORD",
}

const keyTypeSchema = joi.string().valid(...Object.values(KeyType));
const keyValueSchema = joi.string();
const keyNameSchema = joi.string().alphanum().min(1).max(1024);
const captchaSchema = joi.string();

const keyAddValidationSchema = joi.object({
    keyType: keyTypeSchema.required(),
    keyValue: keyValueSchema.required(),
    keyName: keyNameSchema.required()
})

const keyDeleteValidationSchema = joi.object({
    keyType: keyTypeSchema.required(),
    keyValue: keyValueSchema.required(),
    keyName: keyNameSchema.required()
})

const userRegisterSchema = joi.object({
    username: usernameSchema.required(),
    pubKey: pubKeySchema.required(),
    captcha: captchaSchema.required()
})

const challengeRequestSchema = joi.object({
    username: usernameSchema.required()
})

const challengeResponseSchema = joi.object({
    username: usernameSchema.required(),
    signature: signatureSchema.required()
})

const registerSchemaValidator = jsonSchemaValidator(userRegisterSchema, 400)
const keyAddSchemaValidator = jsonSchemaValidator(keyAddValidationSchema, 400)
const keyDeleteSchemaValidator = jsonSchemaValidator(keyDeleteValidationSchema, 400)
const challengeRequestSchemaValidator = jsonSchemaValidator(challengeRequestSchema, 400)
const challengeResponseSchemaValidator = jsonSchemaValidator(challengeResponseSchema, 400)

module.exports = {
    registerSchemaValidator,
    keyAddSchemaValidator,
    keyDeleteSchemaValidator,
    challengeRequestSchemaValidator,
    challengeResponseSchemaValidator
}