const express = require('express');
const router = express.Router();

const {
    getUserSecretKeys,
    removeKeyFromDatabase,
    addKeyToDatabase,
    checkValidWebToken,
    generateAuthChallenge,
    checkAuthChallenge,
    removeChallengeIfValid,
    insertNewUser,
    generateValidWebToken,
    verifyCaptcha
} = require('../middlewares/middlewares')

const {
    keyDeleteSchemaValidator,
    keyAddSchemaValidator,
    registerSchemaValidator,
    challengeRequestSchemaValidator,
    challengeResponseSchemaValidator,
} = require('../validators/validation.schemas')

router.post('/register', registerSchemaValidator, verifyCaptcha, insertNewUser)

router.put('/keys', (req, res, next) => {
    console.log(req.body)
    next()
}, keyAddSchemaValidator, checkValidWebToken, addKeyToDatabase)
router.delete('/keys', keyDeleteSchemaValidator, checkValidWebToken, removeKeyFromDatabase)
router.get('/keys', checkValidWebToken, getUserSecretKeys)

router.post('/request', challengeRequestSchemaValidator, generateAuthChallenge)
router.post('/submit', challengeResponseSchemaValidator, checkAuthChallenge,
    removeChallengeIfValid, generateValidWebToken)

module.exports = router;
