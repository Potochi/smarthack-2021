import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'; 
import { Observable } from 'rxjs';
import { User } from '../models/user';
import { AppSettings } from '../app-settings';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) {}

  registerUser(user: User) {
    return this.http.post(AppSettings.API_URL + 'register', user) as Observable<User>;
  }

  loginUser(user: User) {
    return this.http.post(AppSettings.API_URL + 'login', user) as Observable<any>;
  }

  solveChallenge(username: string, signedChallenge: string) {
    return this.http.post(AppSettings.API_URL + 'submit', {
      "username": username,
      "signature": signedChallenge
    }) as Observable<any>;
  }

  requestChallenge(username: string) {
    return this.http.post(AppSettings.API_URL + 'request', {
      "username": username
    }) as Observable<any>;
  }
}
