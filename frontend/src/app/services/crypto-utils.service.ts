import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CryptoUtilsService {

  constructor() { }

  ab2str(buf: ArrayBuffer) {
    let dec = new TextDecoder('utf-8');

    return dec.decode(new Uint8Array(buf));
  }


  arrayBufferToBase64(buffer: ArrayBuffer) {
    var binary = '';
    var bytes = new Uint8Array(buffer);
    var len = bytes.byteLength;
    for (var i = 0; i < len; i++) {
      binary += String.fromCharCode(bytes[i]);
    }
    return window.btoa(binary);
  }

  str2ab(str: string) {
    const buf = new ArrayBuffer(str.length);
    const bufView = new Uint8Array(buf);
    for (let i = 0, strLen = str.length; i < strLen; i++) {
      bufView[i] = str.charCodeAt(i);
    }
    return buf;
  }
  

  exportCryptoKey(key: ArrayBuffer, type: string) {
    const exportedAsBase64 = this.arrayBufferToBase64(key);
    let pemExported = `-----BEGIN ${type} KEY-----\n${exportedAsBase64}\n-----END ${type} KEY-----`;
    return window.btoa(pemExported);
  }

  convertRsaKey(pem: string, type: string) {
    console.log(pem, type);
    // fetch the part of the PEM string between header and footer
    const pemHeader = `-----BEGIN ${type} KEY-----\n`;
    const pemFooter = `\n-----END ${type} KEY-----`;
    const pemContents = pem.substring(pemHeader.length, pem.length - pemFooter.length);
    console.log(pemContents);
    // base64 decode the string to get the binary data
    const binaryDerString = window.atob(pemContents);
    // convert from a binary string to an ArrayBuffer
    const binaryDer = this.str2ab(binaryDerString);
    return binaryDer;
  }
}
