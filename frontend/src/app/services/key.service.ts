import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppSettings } from '../app-settings';
import { Key } from '../models/key';

@Injectable({
  providedIn: 'root'
})
export class KeyService {

  constructor(private http: HttpClient) { }

  getKeys() {    
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: `Bearer ${localStorage.getItem('token')}`
      })
    };
    return this.http.get(AppSettings.API_URL + 'keys', httpOptions) as Observable<any>;
  }

  deleteKey(key: Key) {
    const httpOptions = {
      body: key,
      headers: new HttpHeaders({
        Authorization: `Bearer ${localStorage.getItem('token')}`
      })
    };
    return this.http.delete(AppSettings.API_URL + 'keys', httpOptions) as Observable<any>;
  }

  insertKey(key: Key) {
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      })
    };

    return this.http.put<Key>(AppSettings.API_URL + 'keys', key, httpOptions) as Observable<any>;
  }
}
