export class User {
    public constructor(username: string, pubKey: string, token: string) {
        this.username = username;
        this.pubKey = pubKey;
        this.captcha = token;
    }
    username: string = '';
    pubKey: string = '';
    captcha: string = '';

}