import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { Key } from '../../models/key';
import { CryptoUtilsService } from '../../services/crypto-utils.service';
import { KeyService } from '../../services/key.service';
import { Clipboard } from '@angular/cdk/clipboard';

@Component({
  selector: 'app-key-storage',
  templateUrl: './key-storage.component.html',
  styleUrls: ['./key-storage.component.css']
})
export class KeyStorageComponent implements OnInit {

  keys: Key[] = [];
  encryptedKeys: Key[] = [];
  keySubscription?: Subscription;
  deleteSubscription?: Subscription;


  keyNameFormControl: FormControl = new FormControl('');
  algorithmFormControl: FormControl = new FormControl('');

  constructor(private keyService: KeyService, private cryptoUtils: CryptoUtilsService, private clipboard: Clipboard) { }
  ngOnInit(): void {
    this.getKeys();
  }

  getKeys() {

    if (typeof this.keySubscription !== "undefined") {
      this.keySubscription.unsubscribe();
    }

    this.keySubscription = this.keyService.getKeys().subscribe((response) => {
      this.keys = response.keys;
      this.encryptedKeys = [];

      for (let k of response.keys) {
        let newKey = new Key();
        newKey.keyName = k.keyName;
        newKey.keyType = k.keyType;
        newKey.keyValue = k.keyValue;

        this.encryptedKeys.push(newKey);
      }
      console.log(this.encryptedKeys);

      let privKeyDer: ArrayBuffer = new ArrayBuffer(0);
      let privKey = localStorage.getItem('priv_key_encrypt');
      if (privKey !== null) {
        privKeyDer = this.cryptoUtils.convertRsaKey(window.atob(privKey), "PUBLIC");
      }
      this.keys.map(function(k: Key) {
        let value = Buffer.from(k.keyValue, 'hex');
        window.crypto.subtle.importKey(
          "pkcs8",
          privKeyDer,
          {
            name: "RSA-OAEP",
            hash: "SHA-256"
          },
          true,
          ["decrypt"]
        ).then(key => {
          window.crypto.subtle.decrypt({
            name: "RSA-OAEP"
          },
          key,
          value
        ).then(decryptedValue => {
          window.crypto.subtle.decrypt({
            name: "RSA-OAEP"
          },
          key,
          Buffer.from(k.keyName, 'hex')).then(decryptedName => {
            k.keyValue = Buffer.from(decryptedValue).toString('hex');
            k.keyName = Buffer.from(decryptedName).toString('utf-8');
            return k;
          });
          
        })
        });
      })
    });
  }

  onCopy(key: Key) {
    this.clipboard.copy(key.keyValue);
  }


  addKey(name: string, type: string, value: ArrayBuffer) {
    let toAdd = new Key();
    toAdd.keyName = name;
    toAdd.keyType = type;


    let privKeyDer: ArrayBuffer = new ArrayBuffer(0);
    let privKey = localStorage.getItem('pub_key_encrypt');
    if (privKey !== null) {
      privKeyDer = this.cryptoUtils.convertRsaKey(window.atob(privKey), "PUBLIC");
    }

    console.log(privKeyDer);

    window.crypto.subtle.importKey(
      "spki",
      privKeyDer,
      {
        name: "RSA-OAEP",
        hash: "SHA-256"
      },
      true,
      ["encrypt"]
    ).then(key => {
      console.log(key);
      window.crypto.subtle.encrypt({
        name: "RSA-OAEP"
      },
      key,
      Buffer.from(value)
    ).then(encryptedValue => {
        window.crypto.subtle.encrypt({
          name: "RSA-OAEP"
        },
        key,
        Buffer.from(name)).then(encryptedName => {
          toAdd.keyName = Buffer.from(encryptedName).toString('hex');
          toAdd.keyValue = Buffer.from(encryptedValue).toString('hex');
          this.keyService.insertKey(toAdd).subscribe(() => {
          this.getKeys();
        });
        })
        
    })
    });
  }

  onAdd() {
    if (this.algorithmFormControl.value === "AES128") {
      let key = window.crypto.subtle.generateKey(
        {
          name: "AES-GCM",
          length: 128
        },
        true,
        ["encrypt", "decrypt"]
      ).then(key => {
        window.crypto.subtle.exportKey("raw", key).then(exported => {
          console.log(Buffer.from(exported).toString('hex'));
          this.addKey(this.keyNameFormControl.value, this.algorithmFormControl.value, exported);
        })
      });
      
    } else if (this.algorithmFormControl.value === "AES192") {
      let key = window.crypto.subtle.generateKey(
        {
          name: "AES-GCM",
          length: 192
        },
        true,
        ["encrypt", "decrypt"]
      ).then(key => {
        window.crypto.subtle.exportKey("raw", key).then(exported => {
          console.log(Buffer.from(exported).toString('hex'));
          this.addKey(this.keyNameFormControl.value, this.algorithmFormControl.value, exported);
        })
      });
    } else if (this.algorithmFormControl.value === "AES256") {
      let key = window.crypto.subtle.generateKey(
        {
          name: "AES-GCM",
          length: 256
        },
        true,
        ["encrypt", "decrypt"]
      ).then(key => {
        window.crypto.subtle.exportKey("raw", key).then(exported => {
          console.log(Buffer.from(exported).toString('hex'));
          this.addKey(this.keyNameFormControl.value, this.algorithmFormControl.value, exported);
        })
      });
    } else {
      alert("Protocol unknown or not implemented");
    }



  }

  onDelete(index: number) {
    console.log(this.encryptedKeys);
    let key = this.encryptedKeys[index];
    this.keyService.deleteKey(key).subscribe(() => {
      this.getKeys();
    });
  }

  ngOnDestroy() {
    if (typeof this.keySubscription !== "undefined") {
      this.keySubscription.unsubscribe();
    }

    if (typeof this.deleteSubscription !== "undefined") {
      this.deleteSubscription.unsubscribe();
    }
  }

}
