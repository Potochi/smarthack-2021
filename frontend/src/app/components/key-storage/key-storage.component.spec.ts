import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KeyStorageComponent } from './key-storage.component';

describe('KeyStorageComponent', () => {
  let component: KeyStorageComponent;
  let fixture: ComponentFixture<KeyStorageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KeyStorageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KeyStorageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
