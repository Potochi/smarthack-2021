import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { User } from '../../models/user';
import { UserService } from '../../services/user.service';
import * as crypto from 'crypto';
import { generateKeyPair } from 'crypto';
import { Buffer } from 'buffer';
import { CryptoUtilsService } from '../../services/crypto-utils.service';
import { Clipboard } from '@angular/cdk/clipboard'
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-register',
  templateUrl: './user-register.component.html',
  styleUrls: ['./user-register.component.css']
})
export class UserRegisterComponent implements OnInit {


  usernameFormControl = new FormControl('');
  passwordFormControl = new FormControl('');
  captchaToken = '';

  publicKey?: CryptoKey;
  privateKey?: CryptoKey;


  constructor(private userService: UserService, private cryptoUtils: CryptoUtilsService, private clipboard: Clipboard, private router: Router) { }

  ngOnInit(): void {
  }

  onVerify(token: string) {
    console.log(token);
    this.captchaToken = token;
  }

  onExpired(response: any) {
    // The verification expired.
    this.captchaToken = '';
  }

  onError(error: any) {
    // An error occured during the verification process.
    this.captchaToken = '';
  }

  onRegister() {
    let keyPair = window.crypto.subtle.generateKey(
      {
        name: "RSASSA-PKCS1-v1_5",
        modulusLength: 2048,
        publicExponent: new Uint8Array([1, 0, 1]),
        hash: "SHA-256"
      },
      true,
      ["sign", "verify"]
    ).then(keypair => {
      console.log(keypair);
      if (keypair.privateKey !== undefined) {
        window.crypto.subtle.exportKey("pkcs8", keypair.privateKey).then((exported) => {
          let privateKey = this.cryptoUtils.exportCryptoKey(exported, "PRIVATE");

          console.log(privateKey);
          localStorage.setItem('priv_key', privateKey);

          if (keypair.publicKey !== undefined) {
          window.crypto.subtle.exportKey("spki", keypair.publicKey).then((exported) => {
            let finalPubKey = this.cryptoUtils.exportCryptoKey(exported, "PUBLIC");
            let user = new User(this.usernameFormControl.value, finalPubKey, this.captchaToken);
            this.userService.registerUser(user).subscribe();

            
            alert("Register successful. Private Key copied to clipboard");
            this.clipboard.copy(privateKey);

            this.router.navigate(['login']);
          });
        }
        });
      }
    });


    let keyPairStorage = window.crypto.subtle.generateKey(
      {
        name: "RSA-OAEP",
        modulusLength: 2048,
        publicExponent: new Uint8Array([1, 0, 1]),
        hash: "SHA-256"
      },
      true,
      ["encrypt", "decrypt"]
    ).then(keypair => {
      console.log(keypair);
      if (keypair.privateKey !== undefined && keypair.publicKey !== undefined) {
        window.crypto.subtle.exportKey("pkcs8", keypair.privateKey).then((exported) => {
          let privateKey = this.cryptoUtils.exportCryptoKey(exported, "PRIVATE");
          localStorage.setItem('priv_key_encrypt', privateKey);
          this.clipboard.copy(privateKey);
        });

        window.crypto.subtle.exportKey("spki", keypair.publicKey).then((exported) => {
          let finalPubKey = this.cryptoUtils.exportCryptoKey(exported, "PUBLIC");
          localStorage.setItem('pub_key_encrypt', finalPubKey);
        });
      }
    });
  }
}
