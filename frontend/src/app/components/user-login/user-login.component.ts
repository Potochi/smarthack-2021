import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from '../../models/user';
import { CryptoUtilsService } from '../../services/crypto-utils.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.css']
})
export class UserLoginComponent implements OnInit {

  usernameFormControl = new FormControl('');
  passwordFormControl = new FormControl('');


  username: string = '';

  
  constructor(private userService: UserService, private router: Router, private cryptoUtils: CryptoUtilsService) { }

  ngOnInit(): void {
  }


  signChallenge(challenge: string) {
    let data = Buffer.from(challenge, 'hex');
    console.log(data);

    
    let privKeyDer: ArrayBuffer = new ArrayBuffer(0);
    let privKey = localStorage.getItem('priv_key');
    if (privKey !== null) {
      privKeyDer = this.cryptoUtils.convertRsaKey(window.atob(privKey), "PRIVATE");
    }

    window.crypto.subtle.importKey("pkcs8",
      privKeyDer,
      {
        name: "RSASSA-PKCS1-v1_5",
        hash: "SHA-256",
      },
      true,
      ["sign"]
    ).then((key) => {
      window.crypto.subtle.sign(
        "RSASSA-PKCS1-v1_5",
        key,
        data
      ).then((signedChallenge) => {
        let toSend = Buffer.from(signedChallenge).toString('hex');
        this.userService.solveChallenge(this.username, toSend).subscribe((resp) => {
          console.log(resp.token);
          localStorage.setItem('token', resp.token);
          this.router.navigate(['keys']).then(window.location.reload);
        });
      })});
  }


  onLogin() {
    this.username = this.usernameFormControl.value;
    this.userService.requestChallenge(this.usernameFormControl.value).subscribe((resp) => {
      this.signChallenge(resp.challenge);
    });
  }
}
