import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { KeyStorageComponent } from './components/key-storage/key-storage.component';
import { UserLoginComponent } from './components/user-login/user-login.component';
import { UserRegisterComponent } from './components/user-register/user-register.component';

const routes: Routes = [
  {path: '', component: UserRegisterComponent},
  {path: 'login', component: UserLoginComponent},
  {path: 'register', component: UserRegisterComponent},
  {path: 'keys', component: KeyStorageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
